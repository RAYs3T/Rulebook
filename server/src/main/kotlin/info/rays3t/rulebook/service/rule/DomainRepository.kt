package info.rays3t.rulebook.service.rule

import org.springframework.data.repository.CrudRepository

interface DomainRepository : CrudRepository<RuleDomain, Int>