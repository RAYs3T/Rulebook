package info.rays3t.rulebook.service.rule

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "rule_segment")
data class RuleSegment(
        @Id
        @Column(name = "segment_id")
        var id: Int,

        /*
        @ManyToOne
        @JoinColumn(name = "parent_segment_id")
        var parentSegment: RuleSegment,

*/

        @ManyToOne(cascade = [(CascadeType.ALL)])
        @JoinColumn(name = "segment_domain_id")
        @JsonIgnore
        var domain: RuleDomain,

        @Column(name = "segment_name")
        var segmentName: String,

        @Column(name = "segment_description")
        var description: String,

        @Column(name = "segment_created")
        var created: Date,

        @OneToMany
        @JoinColumn(name = "rule_segment_id")
        var rules: List<Rule>

)