package info.rays3t.rulebook.service.rule

import com.fasterxml.jackson.annotation.JsonIgnore
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "rule_rule")
data class Rule(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "rule_id")
        var id: Int,

        @ManyToOne
        @JoinColumn(name = "rule_segment_id")
        @JsonIgnore
        var segment: RuleSegment,

        @Column(name = "rule_created")
        var created: Date,

        @Column(name = "rule_changed")
        var changed: Date,

        @OneToMany
        @JoinColumn(name = "rule_rule_id")
        var texts: List<RuleText>
)
