package info.rays3t.rulebook.service.rule

import java.util.*
import javax.persistence.*

@Entity
@Table(name = "rule_domain")
data class RuleDomain(
        @Id
        @Column(name = "domain_id")
        var id: Int,

        @Column(name = "domain_name")
        var name: String,

        @Column(name = "domain_created")
        var created: Date,

        @OneToMany(mappedBy = "domain")
        var segment: List<RuleSegment>


)